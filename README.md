# Introduction

A Pleroma/Mastodon client, focus on editing experience (WIP 🏗️), developed by [@seviche@kongwoo.icu](https://kongwoo.icu/seviche)

## Features

- Markdown
- drop and drag block

## Development

```bash
pnpm install
```

```bash
pnpm dev
```